<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 1:11 PM
 */

return [

    'client_id' => env('VKONTAKTE_CLIENT_ID', 'null'),

    'client_secret' => env('VKONTAKTE_CLIENT_SECRET', 'null'),

    'redirect_uri' => env('VKONTAKTE_REDIRECT_URI', 'https://oauth.vk.com/blank.html'),

    'code' => env('VKONTAKTE_CODE', 'null'),

    'token' => env('VKONTAKTE_TOKEN', 'null')

];
