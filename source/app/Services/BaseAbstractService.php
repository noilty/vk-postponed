<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 2:29 PM
 */

namespace App\Services;

use App\Entities\Vkontakte\Settings;

abstract class BaseAbstractService
{
    protected Settings $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }
}
