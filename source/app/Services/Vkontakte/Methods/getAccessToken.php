<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 3:52 PM
 */

namespace App\Services\Vkontakte\Methods;

use App\Entities\Vkontakte\Settings;
use VK\Client\VKApiClient;
use VK\Exceptions\VKClientException;
use VK\Exceptions\VKOAuthException;
use VK\OAuth\Scopes\VKOAuthGroupScope;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

class getAccessToken extends \App\Services\Vkontakte\VkontakteAbstractService
{
    public function __construct(Settings $settings, VKApiClient $client, VKOAuth $oauth, VKOAuthDisplay $oauthDisplay, VKOAuthUserScope $oauthUserScope, VKOAuthGroupScope $oauthGroupScope, VKOAuthResponseType $oauthResponseType)
    {
        parent::__construct($settings, $client, $oauth, $oauthDisplay, $oauthUserScope, $oauthGroupScope, $oauthResponseType);
    }

    /**
     * access_token
     *
     * @return string
     */
    final public function get(): string
    {
        try
        {
            return $this->oauth->getAccessToken(
                $this->settings->getClientId(),
                $this->settings->getClientSecret(),
                $this->settings->getRedirectUri(),
                $this->settings->getCode()
            )['access_token'];
        }
        catch (VKClientException|VKOAuthException $e)
        {
            return 'Oops, who this?';
        }
    }
}
