<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 2:50 PM
 */

namespace App\Services\Vkontakte\Methods;

use App\Entities\Vkontakte\Settings;
use VK\Client\VKApiClient;
use VK\OAuth\Scopes\VKOAuthGroupScope;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

class getAccessKey extends \App\Services\Vkontakte\VkontakteAbstractService
{
   public function __construct(Settings $settings, VKApiClient $client, VKOAuth $oauth, VKOAuthDisplay $oauthDisplay, VKOAuthUserScope $oauthUserScope, VKOAuthGroupScope $oauthGroupScope, VKOAuthResponseType $oauthResponseType)
   {
       parent::__construct($settings, $client, $oauth, $oauthDisplay, $oauthUserScope, $oauthGroupScope, $oauthResponseType);
   }

    /**
     * browser_url
     *
     * @return string
     */
    final public function get(): string
    {
        return $this->oauth->getAuthorizeUrl(
            $this->oauthResponseType::CODE,
            $this->settings->getClientId(),
            $this->settings->getRedirectUri(),
            $this->oauthDisplay::PAGE,
            array($this->oauthUserScope::WALL, $this->oauthGroupScope::PHOTOS)
        );
    }
}
