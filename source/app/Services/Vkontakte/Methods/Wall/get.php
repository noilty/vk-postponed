<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 2:37 PM
 */

namespace App\Services\Vkontakte\Methods\Wall;

use VK\Exceptions\Api\VKApiBlockedException;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class get extends \App\Services\Vkontakte\VkontakteAbstractService
{
    /**
     * @throws VKApiBlockedException
     * @throws VKApiException
     * @throws VKClientException
     */
    public function __invoke(array $params)
    {
        $token = $this->settings->getToken();

        return $this->client->wall()->get($token, $params);
    }
}
