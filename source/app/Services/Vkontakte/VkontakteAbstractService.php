<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 2:53 PM
 */

namespace App\Services\Vkontakte;

use App\Entities\Vkontakte\Settings;
use App\Services\BaseAbstractService;
use VK\Client\VKApiClient;
use VK\OAuth\Scopes\VKOAuthGroupScope;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

abstract class VkontakteAbstractService extends BaseAbstractService
{
    protected VKApiClient $client;

    protected VKOAuth $oauth;

    protected VKOAuthDisplay $oauthDisplay;

    protected VKOAuthUserScope $oauthUserScope;

    protected VKOAuthGroupScope $oauthGroupScope;

    protected VKOAuthResponseType $oauthResponseType;

    public function __construct
    (
        Settings $settings,
        VKApiClient $client,
        VKOAuth $oauth,
        VKOAuthDisplay $oauthDisplay,
        VKOAuthUserScope $oauthUserScope,
        VKOAuthGroupScope $oauthGroupScope,
        VKOAuthResponseType $oauthResponseType
    )
    {
        parent::__construct($settings);
        $this->client = $client;
        $this->oauth = $oauth;
        $this->oauthDisplay = $oauthDisplay;
        $this->oauthUserScope = $oauthUserScope;
        $this->oauthGroupScope = $oauthGroupScope;
        $this->oauthResponseType = $oauthResponseType;
    }
}
