<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 12:49 PM
 */

namespace App\Entities\Vkontakte;


class Settings
{
    use \App\Entities\Vkontakte\Extensions\SettingsTrait;

    protected int $client_id;

    protected string $client_secret;

    protected string $redirect_uri;

    protected string $code;

    protected string $token;

    public function __construct()
    {
        $this->client_id = config('vkontakte.client_id');
        $this->client_secret = config('vkontakte.client_secret');
        $this->redirect_uri = config('vkontakte.redirect_uri');
        $this->code = config('vkontakte.code');
        $this->token = config('vkontakte.token');
    }

}
