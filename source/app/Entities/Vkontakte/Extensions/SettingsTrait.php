<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 1:26 PM
 */

namespace App\Entities\Vkontakte\Extensions;

trait SettingsTrait
{
    use BaseTrait;

    public function getClientId(): int
    {
        return $this->client_id;
    }

    public function getClientSecret(): string
    {
        return $this->client_secret;
    }

    public function getRedirectUri(): string
    {
        return $this->redirect_uri;
    }

    protected function getCode(): string
    {
        return $this->code;
    }

    protected function getToken(): string
    {
        return $this->token;
    }
}
