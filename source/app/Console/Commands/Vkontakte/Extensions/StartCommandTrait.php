<?php

/*
 * Автор Noilty
 * https://t.me/Noilty
 *
 * Пользователь: r.kuzmin
 * Дата: 2022-11-12
 * Время: 1:40 PM
 */

namespace App\Console\Commands\Vkontakte\Extensions;

trait StartCommandTrait
{
    use BaseTrait;

    private string $group_name;

    private function setGroupName(): void
    {
        $this->group_name = $this->argument('GROUP_NAME');
    }

    private function getGroupName(): string
    {
        return $this->group_name;
    }
}
