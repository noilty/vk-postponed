<?php

namespace App\Console\Commands\Vkontakte;

use App\Entities\Vkontakte\Settings;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;

class StartCommand extends Command
{
    use \App\Console\Commands\Vkontakte\Extensions\StartCommandTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vk:start {GROUP_NAME}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Settings
     */
    protected Settings $settings;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Settings $settings)
    {
        parent::__construct();
        $this->settings = $settings;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        //$this->setGroupName();

        //echo $this->settings->getClientId() . PHP_EOL;

        return CommandAlias::SUCCESS;
    }
}
